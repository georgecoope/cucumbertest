package stepdefs;

import cucumber.api.java.en.Given;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class DemoDefinitions {

    @Given(value = "I get a response from all hosts", timeout = 5000)
    public void i_get_a_response_from(List<String> ips) {

        List<String> failedIPs = new ArrayList<String>();
        int discoveredHosts = 0;

        for (String ip : ips) {
            try {
                System.out.println("Pinging: " + ip);

                InetAddress target = InetAddress.getByName(ip);

                assertNotNull("Destination host isn't null", target);

                if (target != null) {
                    discoveredHosts++;
                    if (!target.isReachable(5000)) {
                        failedIPs.add(ip);
                    }
                } else {
                    System.out.println("IP target was null for ip: " + ip);
                }
            } catch (Exception e) {
                System.out.println(e);
            }
        }

        if(failedIPs.size() > 0) {
            for (String failedIP : failedIPs) {
                System.out.println("Host failed to respond " + failedIP);
            }
        }

        assertTrue("There are no failed IPs", failedIPs.isEmpty());
        assertEquals("Passed ip count should equal discovered hosts count", ips.size(), discoveredHosts);
    }

}
