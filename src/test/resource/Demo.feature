Feature: Can we reach new host

  Scenario: Ping the machine
    Given I get a response from all hosts
    | 1.1.1.1 |
    | 8.8.8.8 |
    | 127.0.0.1 |
    | google.com |